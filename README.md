**Desafioflex**
**Version 1.0.0**

Platform designed to manage certificates, in which it's possible to register, edit and delete. This is only the backend API.

**Getting started**

1. git clone [Desafioflex](https://gitlab.com/ericalaboi/kmdb.git)
2. cd kmdb
3. docker-compose up

**Deploying / Publishing**
The project was deployed on heroku
[Click here to visit the project](https://desafioflex.herokuapp.com/)

**Routes**

- POST /api/certificate/ - creates certificate
- POST /api/groups/ - creates groups
- GET /api/certificate/ - list all certificates and their groups
- GET /api/certificate/ - filters certificates by username and name
- PUT /api/certificate/pk/ - updates certificate data
- DELETE /api/certificate/pk/ - delete certificates using primary key

**Examples of Requests:**

- Creating certificate: POST /api/certificate/
>  {
> "name": "Fernanda",
> "username": "Fe",
> "description": "Test",
> "expiration": 100
> }


- Creating groups: POST /api/groups
> {
> "groups": 15,
> "certificate": 1
> }


- Getting by name: GET /api/certificate
>  {
> "name": "Fernanda"
> }


- Getting by username: GET /api/certificate
> {
> "username": "Fe"
> }


- Changing a certificate: PUT /api/certificate/pk
> {
> "name": "Fernanda",
> "username": "Nanda",
> "description": "Test",
> "expiration": 20
> }


**Deleting a certificate**: DELETE /api/certificate/pk

**Running the tests:**
Execute TEST=TEST python manage.py test
