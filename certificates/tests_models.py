
from datetime import datetime, timedelta, tzinfo
import pytz
from os import replace
from django.test import TestCase
from .models import Certificate, Groups


class CertificateModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.created_at = datetime.now()
        cls.expirated_at = datetime.now() + timedelta(days=30)
        cls.updated_at = datetime.utcnow().replace(tzinfo=pytz.utc)
        cls.certificate = Certificate.objects.create(
            username="Fer",
            name="Fernanda",
            description="Certificado de RH",
            expiration=30
            # expirated_at= calculado
            # created_at= automatico
            # updated_at= automatico
        )

    def test_iput_model_fields(self):
        self.assertIsInstance(self.certificate.username, str)
        self.assertEqual(self.certificate.username, "Fer")

        self.assertIsInstance(self.certificate.name, str)
        self.assertEqual(self.certificate.name, "Fernanda")

        self.assertIsInstance(self.certificate.description, str)
        self.assertEqual(self.certificate.description, "Certificado de RH")

        self.assertIsInstance(self.certificate.expiration, int)
        self.assertEqual(self.certificate.expiration, 30)

        self.assertIsInstance(self.certificate.created_at, datetime)
        self.assertEqual(self.certificate.created_at.replace(
            microsecond=0), self.created_at.replace(microsecond=0))

        self.assertIsInstance(self.certificate.expirated_at, datetime)
        self.assertEqual(self.certificate.expirated_at.replace(microsecond=0),
                         self.expirated_at.replace(microsecond=0))

        self.assertIsInstance(self.certificate.updated_at, datetime)
        self.assertEqual(self.certificate.updated_at.replace(
            microsecond=0), self.updated_at.replace(microsecond=0))


class GroupsModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.certificate = Certificate.objects.create(
            username="Fer",
            name="Fernanda",
            description="Certificado de RH",
            expiration=30

        )
        cls.group = Groups.objects.create(
            groups=15,
            certificate_id=1

        )

    def test_iput_model_fields(self):
        self.assertIsInstance(self.group.groups, int)
        self.assertEqual(self.group.groups, 15)
