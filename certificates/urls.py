from django.urls import path
from .views import CertificateListCreateView, GroupsListCreateView, CertificateDestroyView

urlpatterns = [
    path('certificate/<pk>/', CertificateDestroyView.as_view()),
    path('certificate/', CertificateListCreateView.as_view()),
    path('groups/', GroupsListCreateView.as_view())

]
