from django.db import models
from datetime import timedelta
from datetime import datetime
from django.core.validators import MaxValueValidator, MinValueValidator
import ipdb


class Certificate(models.Model):

    username = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    expiration = models.IntegerField(
        validators=[MinValueValidator(10), MaxValueValidator(3650)])
    expirated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(default=datetime.now)
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True)

    def save(self, *args, **kwargs):
        if not self.pk:

            self.expirated_at = self.created_at + \
                timedelta(days=self.expiration)
        super(Certificate, self).save(*args, **kwargs)


class Groups(models.Model):
    GROUPS = (
        ("1", "ADM"),
        ("15", "COMERCIAL"),
        ("30", "RH"),
    )

    groups = models.CharField(max_length=30, choices=GROUPS)
    certificate = models.ForeignKey(
        Certificate, on_delete=models.CASCADE, related_name='group')
