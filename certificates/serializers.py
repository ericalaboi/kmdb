from rest_framework import serializers
from .models import Certificate, Groups


class GroupsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Groups
        fields = ['groups', 'id', 'certificate']


class CertificateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Certificate
        fields = ['id', 'username', 'name',  'description',
                  'group', 'expiration', 'created_at', 'expirated_at', 'updated_at']
        extra_kwargs = {
            'group': {'read_only': True}, 'expirated_at': {'read_only': True}, 'created_at': {'read_only': True}, 'updated_at': {'read_only': True}
        }

    group = GroupsSerializer(many=True, read_only=True)
