from rest_framework.generics import ListCreateAPIView, UpdateAPIView, DestroyAPIView
from rest_framework.response import Response
from .models import Certificate, Groups
from .serializers import CertificateSerializer, GroupsSerializer


class MultipleFildLookupMixin:
    def get_queryset(self):
        queryset = self.queryset
        lookup_filter = {}
        for lookup_fild in self.lookup_filds:
            if self.request.data.get(lookup_fild):
                lookup_filter[f'{lookup_fild}__icontains'] = self.request.data.get(
                    lookup_fild)

        queryset = queryset.filter(**lookup_filter)
        return queryset


class CertificateListCreateView(MultipleFildLookupMixin, ListCreateAPIView):
    serializer_class = CertificateSerializer
    queryset = Certificate.objects.all()
    lookup_filds = ['name', 'username']


class CertificateDestroyView(DestroyAPIView, UpdateAPIView):
    serializer_class = CertificateSerializer
    queryset = Certificate.objects.all()


class GroupsListCreateView(ListCreateAPIView):
    serializer_class = GroupsSerializer
    queryset = Groups.objects.all()
